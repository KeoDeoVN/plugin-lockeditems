package mk.plugin.lockeditems.main;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.milkbowl.vault.economy.Economy;

public enum PriceType {
	
	MONEY {
		@Override
		public double getAsset(Player player) {
			if (Bukkit.getPluginManager().isPluginEnabled("Vault")) {
				Economy eco = (Economy) Bukkit.getServer().getServicesManager().getRegistration(Economy.class).getProvider();
				return eco.getBalance(player);
			}
			return 0;
		}

		@Override
		public boolean pay(Player player, double value) {
			if (!canPay(player, value)) return false;
			Economy eco = (Economy) Bukkit.getServer().getServicesManager().getRegistration(Economy.class).getProvider();
			eco.withdrawPlayer(player, value);
			return true;
		}
	};
	
	public abstract double getAsset(Player player);
	public abstract boolean pay(Player player, double value);
	
	public boolean canPay(Player player, double value) {
		return getAsset(player) >= value;
	}
	
}
