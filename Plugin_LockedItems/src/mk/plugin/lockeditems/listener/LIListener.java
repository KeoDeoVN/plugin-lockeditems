package mk.plugin.lockeditems.listener;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import me.Zrips.TradeMe.TradeMe;
import mk.plugin.lockeditems.gui.LIGUI;
import mk.plugin.lockeditems.main.MainLockedItems;
import mk.plugin.lockeditems.utils.LIUtils;

public class LIListener implements Listener {
	
	@EventHandler
	public void onGUI(InventoryClickEvent e) {
		LIGUI.eventClick(e);
	}
	
	@EventHandler
	public void onGUI(InventoryCloseEvent e) {
		LIGUI.eventClose(e);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		LIUtils.checkInventory(player);
	}

	@EventHandler
	public void onCloseInventory(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		LIUtils.checkInventory(player);
	}

	@EventHandler
	public void onHeldItem(PlayerItemHeldEvent e) {
		Player player = e.getPlayer();
		LIUtils.checkInventory(player);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onDrop(PlayerDropItemEvent e) {
		if (!e.isCancelled()) {
			ItemStack i = e.getItemDrop().getItemStack();
			Player p = e.getPlayer();
			LIUtils.addOwner(i, p.getName());
		}

	}

	@EventHandler
	public void onPickup(EntityPickupItemEvent e) {
		ItemStack i = e.getItem().getItemStack();
		if (e.getEntityType() == EntityType.PLAYER) {
			Player player = (Player) e.getEntity();
			if (!player.hasPermission("lockeditems.ignore")) {
				if (LIUtils.isLocked(i) && !LIUtils.isOwner(i, player.getName())) {
					e.setCancelled(true);
				} else {
					LIUtils.removeOwner(i, player.getName());
				}
			}

		}
	}

	@EventHandler
	public void onItemFrame(PlayerInteractEntityEvent e) {
		Player player = e.getPlayer();
		ItemStack i = player.getInventory().getItemInMainHand();
		Entity et = e.getRightClicked();
		if (LIUtils.isLocked(i) && (et instanceof ItemFrame || player.isSneaking())) {
			LIUtils.addOwner(i, player.getName());
		}

	}
	
	@EventHandler
	public void onEquipArmorStand(PlayerInteractAtEntityEvent e) {
		Player player = e.getPlayer();
		ItemStack i = player.getInventory().getItemInMainHand();
		Entity et = e.getRightClicked();
		if (LIUtils.isLocked(i) && et instanceof ArmorStand) {
			LIUtils.addOwner(i, player.getName());
		}

	}

	@EventHandler
	public void onEquipArmorStand(PlayerArmorStandManipulateEvent e) {
		Player player = e.getPlayer();
		ItemStack i = e.getArmorStandItem();
		if (!LIUtils.isOwner(i, player.getName())) {
			e.setCancelled(true);
			player.sendMessage("§cKhông phải đồ bạn!");
		}

	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDeath(PlayerDeathEvent e) {
		Player player = e.getEntity();
		if (!e.getKeepInventory()) {
			List<ItemStack> list = Lists.newArrayList(e.getDrops());
			for (ItemStack item : list) {
				if (LIUtils.isLocked(item) && !LIUtils.haveOwnerName(item)) {
					e.getDrops().remove(item);
					LIUtils.addOwner(item, player.getName());
					player.getWorld().dropItemNaturally(player.getLocation(), item);
				}
			}

		}
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		ClickType clicktype = e.getClick();
		if (clicktype.equals(ClickType.NUMBER_KEY)) {
			e.setCancelled(true);
		} else {
			InventoryType it = e.getInventory().getType();
			ItemStack cur;
			ItemStack click;
			if (!it.equals(InventoryType.CHEST) && !it.equals(InventoryType.HOPPER) && !it.equals(InventoryType.DROPPER)
					&& !it.equals(InventoryType.DISPENSER) && !it.equals(InventoryType.BREWING)
					&& !it.equals(InventoryType.FURNACE) && !it.equals(InventoryType.SHULKER_BOX)) {
				if (it.equals(InventoryType.ANVIL) && e.getRawSlot() == 2) {
					ItemStack i = e.getInventory().getItem(1);
					cur = e.getInventory().getItem(0);
					click = e.getInventory().getItem(2);
					if (LIUtils.isLocked(i) && !i.getType().equals(Material.BOOK)
							&& !i.getType().equals(Material.ENCHANTED_BOOK)) {
						e.setCancelled(true);
					}

					if (cur != null && cur.hasItemMeta() && click != null && click.hasItemMeta()
							&& click.getItemMeta().hasDisplayName()) {
						String name = click.getItemMeta().getDisplayName();
						ItemMeta im = cur.getItemMeta();
						if (im.hasDisplayName() && !im.getDisplayName().equals(name) || !im.hasDisplayName()) {
							e.setCancelled(true);
						}
					}

					if (LIUtils.isLocked(cur) && !click.getItemMeta().hasDisplayName()) {
						e.setCancelled(true);
					}
				}

			} else {
				Player player = (Player) e.getWhoClicked();
				if (!player.hasPermission("lockeditems.ignore")) {
					cur = e.getCursor();
					click = e.getCurrentItem();
					if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
						if (LIUtils.isLocked(click)) {
							if (!LIUtils.isOwner(click, player.getName())) {
								e.setCancelled(true);
							} else {
								if (e.isShiftClick() && LIUtils.haveOwnerName(click)) {
									LIUtils.removeOwner(click, player.getName());
								}

							}
						}
					} else if (!LIUtils.isOwner(click, player.getName())) {
						e.setCancelled(true);
					} else {
						LIUtils.addOwner(click, player.getName());
						if (LIUtils.isLocked(cur) && LIUtils.haveOwnerName(cur)) {
							e.setCancelled(true);
							if (e.getClickedInventory() != null && LIUtils.isOwner(cur, player.getName())) {
								LIUtils.removeOwner(cur, player.getName());
								player.setItemOnCursor(click);
								player.getInventory().setItem(e.getSlot(), cur);
								player.updateInventory();
								return;
							}
						}

					}
				}
			}
		}
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		Player player = e.getPlayer();
		ItemStack item = player.getInventory().getItemInMainHand();
		if (item != null && !item.getType().equals(Material.AIR) && LIUtils.isLocked(item)) {
			String command = e.getMessage().substring(1).split(" ")[0];
			if (LIUtils.containsCommand(command, LIUtils.BLOCKED_COMMANDS) || command.contains(":")) {
				e.setCancelled(true);
				player.sendMessage("§cKhông thể sử dụng lệnh này khi đang cầm item khóa!");
			}
		}

	}

	@EventHandler
	public void onClickTradeMe(InventoryClickEvent e) {
		if (MainLockedItems.HAS_TRADEME) {
			ItemStack item = e.getCursor();
			Player player = (Player) e.getWhoClicked();
			if (e.getSlot() == 40 && e.getInventory().getType() == InventoryType.CRAFTING
					&& !LIUtils.isOwner(item, player.getName())) {
				e.setCancelled(true);
				player.sendMessage("§cĐồ này không phải của bạn huhu");
			}

			if (e.getClickedInventory() == e.getWhoClicked().getOpenInventory().getTopInventory()
					&& LIUtils.isLocked(item) && TradeMe.getInstance().getGUIManager().isOpenedGui(player)) {
				e.setCancelled(true);
				player.sendMessage("§cKhông thể giao dịch đồ khóa");
			}

		}
	}

	@EventHandler
	public void onClickTradeMe2(InventoryDragEvent e) {
		if (MainLockedItems.HAS_TRADEME) {
			Player player = (Player) e.getWhoClicked();
			if (TradeMe.getInstance().getGUIManager().isOpenedGui(player)) {
				e.setCancelled(true);
				player.sendMessage("§cKhông thể rải đồ khi trade!");
			}

		}
	}

	@EventHandler
	public void onTradeShiftClickTradeMe(InventoryOpenEvent e) {
		if (MainLockedItems.HAS_TRADEME) {
			Player player = (Player) e.getPlayer();
			Bukkit.getScheduler().runTaskAsynchronously(MainLockedItems.getMain(), () -> {
				if (TradeMe.getInstance().getGUIManager().isOpenedGui(player)) {
					TradeMe.getInstance().getGUIManager().getGui(player).setAllowShift(false);
				}

			});
		}
	}
}
