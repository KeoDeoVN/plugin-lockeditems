package mk.plugin.lockeditems.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import mk.plugin.lockeditems.main.MainLockedItems;
import mk.plugin.lockeditems.main.PriceType;
import mk.plugin.lockeditems.utils.LIUtils;

public class LIGUI {
	
	public static final String TITLE = "§c§lKHÓA VẬT PHẨM";
	
	private static final int ITEM_SLOT = 1;
	private static final int BUTTON_SLOT = 7;
	
	public static void openGUI(Player player, ItemStack item) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTask(MainLockedItems.getMain(), () -> {
			player.playSound(player.getLocation(), Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aBấm để thực hiện", "§6Giá: §f" + LIUtils.LOCK_FEE + "$"})));
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(DyeColor.GREEN, "§aÔ chứa §oVật phẩm"));
			if (item != null) {
				inv.setItem(ITEM_SLOT, item);
			}
		});
	}
	
	public static void openGUI(Player player) {
		openGUI(player, null);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				
				// Get item
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				if (item == null) {
					player.sendMessage("§aĐặt vật phẩm vào đi bạn iu");
					return;
				}
				if (LIUtils.isLocked(item)) {
					player.sendMessage("§aVật phẩm này khóa rồi mà bạn iu");
					return;
				}
				
				if (!PriceType.MONEY.canPay(player, LIUtils.LOCK_FEE)) {
					player.sendMessage("§cBạn không đủ khả năng chi trả");
					return;
				}
				PriceType.MONEY.pay(player, LIUtils.LOCK_FEE);
				
				// Do
				LIUtils.lock(item);
				player.getInventory().addItem(item);
				inv.setItem(ITEM_SLOT, null);
				player.closeInventory();
				player.sendMessage("§aKhóa thành công");
				
				player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
			}
		} else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Place 
				if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
					player.sendMessage("§cĐã để Vật phẩm rồi");
					return;
				}
				
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;

		if (!GUIUtils.checkCheck(player)) return;

		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			player.getInventory().addItem(item);
		});
	}
}
