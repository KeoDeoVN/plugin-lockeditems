package mk.plugin.lockeditems.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import mk.plugin.lockeditems.main.MainLockedItems;

public class GUIUtils {
	
	public static String SLOT_ITEM_TAG = "iSlotItem";
	
	public static boolean isItemSlot(ItemStack item) {
		return ItemStackUtils.hasTag(item, SLOT_ITEM_TAG);
	}
	
	public static void addCheck(Player player) {
		player.setMetadata("conmeasdasda", new FixedMetadataValue(MainLockedItems.getMain(), ""));
	}
	
	public static void removeCheckGUI(Player player) {
		player.removeMetadata("conmeasdasda", MainLockedItems.getMain());
	}
	
	public synchronized static boolean checkCheck(Player player) {
		if (player.hasMetadata("conmeasdasda")) {
			removeCheckGUI(player);
			return false;
		} 
		addCheck(player);
		
		Bukkit.getScheduler().runTaskLater(MainLockedItems.getMain(), () -> {
			if (player.hasMetadata("conmeasdasda")) {
				removeCheckGUI(player);
			}
		}, 2);
		return true;
	}
	
	public static ItemStack getButton(List<String> desc) {
		ItemStack item = new ItemStack(Material.CONCRETE);
		item.setDurability(getColor(DyeColor.RED));
		ItemMeta meta = item.getItemMeta();
		if (desc.size() > 0) {
			meta.setDisplayName(desc.get(0));
			if (desc.size() > 1) {
				List<String> lore = new ArrayList<String> ();
				for (int i = 1 ; i < desc.size() ; i++) {
					lore.add(desc.get(i));
				}
				meta.setLore(lore);
			}
		}
		
		item.setItemMeta(meta);
		return item;
	}
	
	public static void updateChance(ItemStack item, double chance) {
		chance = chance > 100 ? 100 : chance;
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("§aTỉ lệ thành công: §f" + chance + "%");
	}
	
	
	public static ItemStack getItemSlot(ItemStack item, String desc) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(desc);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		item.setItemMeta(meta);
		item = ItemStackUtils.setTag(item, SLOT_ITEM_TAG, "");
		return item;
	}
	
	public static ItemStack getItemSlot(DyeColor color, String desc) {
		ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1, getColor(color));
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(desc);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		item.setItemMeta(meta);
		item = ItemStackUtils.setTag(item, SLOT_ITEM_TAG, "");
		return item;
	}
	
	public static ItemStack getSlot(ItemStack item, String desc) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(desc);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack getBlackSlot() {
		ItemStack other = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemMeta meta = other.getItemMeta();
		meta.setDisplayName(" ");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		other.setItemMeta(meta);
		return other;
	}
	
	public static boolean placeItem(ItemStack item, int amount, int currentSlot, Inventory inv, Inventory currentInv, List<Integer> slots) {
		if (item == null || item.getType() == Material.AIR) {
			return false;
		}
		if (amount > item.getAmount()) return false;
		for (int i = 0 ; i < slots.size() ; i++) {
			ItemStack iSlot = inv.getItem(slots.get(i));
			if (ItemStackUtils.hasTag(iSlot, SLOT_ITEM_TAG)) {
				// Chỉ bỏ 1 item vào
				ItemStack clone = item.clone();
				clone.setAmount(amount);
				inv.setItem(slots.get(i), clone);
				// Giảm 1 item
				item = ItemStackUtils.subtractItem(item, amount);
				currentInv.setItem(currentSlot, item);
				// Done
				return true;
			}
			continue;
		}
		return false;
	}
	
	
	public static boolean placeItem(ItemStack item, int currentSlot, Inventory inv, Inventory currentInv, List<Integer> slots) {
		return placeItem(item, 1, currentSlot, inv, currentInv, slots);
	}
	
	public static boolean placeItem(ItemStack item, int currentSlot, Inventory inv, Inventory currentInv, int slot) {
		return placeItem(item, currentSlot, inv, currentInv, Arrays.asList(new Integer[] {slot}));
	}
	
	public static boolean placeItem(ItemStack item, int amount, int currentSlot, Inventory inv, Inventory currentInv, int slot) {
		return placeItem(item, amount, currentSlot, inv, currentInv, Arrays.asList(new Integer[] {slot}));
	}
	
	public static ItemStack getItem(Inventory inv, int slot) {
		ItemStack item = inv.getItem(slot);
		if (ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return null;
		return item;
	}
	
	@SuppressWarnings("deprecation")
	public static short getColor(DyeColor color) {
		return color.getWoolData();
	}

	
	
	
	
	
	
	
	
	
}
